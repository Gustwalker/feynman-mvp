import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase/app'
import 'firebase/auth'
import VueFirestore from 'vue-firestore'


Vue.config.productionTip = false

Vue.use(VueFirestore)

// change workbox-build mode to injectManifest
injectManifest({
	swSrc: 'src/sw.js',
	swDest: 'dist/sw.js',
})

// register the service worker when the site loads
if ('serviceWorker' in navigator) {
	window.addEventListener('load', function() {
		navigator.serviceWorker.register('/sw.js')
	})
}


firebase.auth().onAuthStateChanged(user => {
  if (user) {
    store.commit('SET_USER', user)
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
